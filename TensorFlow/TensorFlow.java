package org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.TensorFlow;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus;

import java.util.List;

import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.DRIVE_SPEED;
import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.kD;
import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.kI;
import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.kP;

@TeleOp(name = "Tensor", group = "Test")
//@Disabled
public class TensorFlow extends LinearOpMode {

    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";

    private ElapsedTime runtime = new ElapsedTime();
    private HardwareZeus zeus = new HardwareZeus();

    private String SIDE = null;

    private VuforiaLocalizer vuforia;
    private TFObjectDetector tfod;

    int goldMineralX = -1;
    double angle = 20.0;

    private double lastError, integralError;
    private double goalAngle = 0.0;

    private Acceleration gravity = null;


    @Override
    public void runOpMode() {

        zeus.init(hardwareMap);

        initVuforia();

        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        }

        zeus.motorRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        zeus.motorLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        telemetry.addData("Ready", "Press > to start");
        telemetry.update();

        waitForStart();

        if (opModeIsActive()) {
            if (tfod != null) {
                tfod.activate();
            }

            gravity = zeus.imu.getGravity();

            zeus.motorLift.setPower(0.4);
            runtime.reset();
            while (gravity.yAccel > -9.8 && runtime.seconds() < 5.0) {
                gravity = zeus.imu.getGravity();
                telemetry.addData("Gravity", gravity.yAccel);
                telemetry.update();
            }
            zeus.motorLift.setPower(0.0);

            zeus.motorLeft.setPower(-DRIVE_SPEED);
            zeus.motorRight.setPower(DRIVE_SPEED);
            runtime.reset();
            while (runtime.seconds() < 0.96) {
                telemetry.addData("Zeus", "is Moving");
                telemetry.update();
            }
            zeus.motorRight.setPower(0.0);
            zeus.motorLeft.setPower(0.0);

            sleep(1000);

            TENSORPID :
            while (opModeIsActive()) {
                if (tfod != null) {
                    runtime.reset();
                    while (runtime.seconds() < 3.0) {
                        telemetry.addData("Looking", "For Cube");
                        telemetry.update();
                    }
                    List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                    if (updatedRecognitions != null) {
                        telemetry.addData("# Object Detected", updatedRecognitions.size());
                        for (Recognition recognition : updatedRecognitions) {
                            if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                                goldMineralX = (int) recognition.getLeft();
                                angle = recognition.estimateAngleToObject(AngleUnit.DEGREES);
                                telemetry.addData("Test", "123");
                                telemetry.update();
                                SIDE = "CENTER";
                                break TENSORPID;
                            } else {
                                telemetry.addData("No Cube", recognition.getConfidence());
                                telemetry.update();
                            }
                        }
                    }

                    zeus.motorRight.setPower(-0.2);
                    zeus.motorLeft.setPower(0.2);
                    runtime.reset();
                    while (runtime.seconds() < 1.2) {
                        telemetry.addData("Zeus", "is Moving");
                        telemetry.update();
                    }
                    zeus.motorRight.setPower(0.0);
                    zeus.motorLeft.setPower(0.0);
                    runtime.reset();
                    while(runtime.seconds() < 3.0) {
                        telemetry.addData("Looking", "For Cube");
                        telemetry.update();
                    }
                    List<Recognition> updatedRecognition = tfod.getUpdatedRecognitions();
                    if (updatedRecognition != null) {
                        for (Recognition recognition : updatedRecognition) {
                            if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                                SIDE = "LEFT";
                                break TENSORPID;
                            } else {
                                telemetry.addData("No Cube", recognition.getConfidence());
                                telemetry.update();
                            }
                        }
                    }
                    zeus.motorRight.setPower(0.2);
                    zeus.motorLeft.setPower(-0.2);
                    runtime.reset();
                    while (runtime.seconds() < 1.9) {
                        SIDE = "RIGHT";
                        telemetry.addData("Zeus", "is Moving");
                        telemetry.update();
                    }
                    zeus.motorRight.setPower(0.0);
                    zeus.motorLeft.setPower(0.0);
                }
            }
            tensorDrive(0.2, 5, 13.5);
            if (SIDE.equals("RIGHT")) {

            } else if (SIDE.equals("LEFT")) {

            } else if (SIDE.equals("CENTER")) {

            }
        }

        if (tfod != null) {
            tfod.shutdown();
        }
    }

    private void initVuforia() {
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = "AWydOn3/////AAAAGWB2YP4r2ERKmLdFMt7DzdUYnt2f97VKdK1fMvb8c5p8iGeDLgwB9dic+osr9GAHQK3K4uJV/8yxon7KXrJNbgzKN82yuHucjwS7gmWkItkoSB+nTn/66dfKF6OyRhh7vBtZqg70Tpv3Pq75kIeij++F34cQNAA3fWEzIoPnuQkew/QP1NNjyZtnIY4lYZFEHgljmtmIP7qwM5vw5pIQRriTaDAfwWPJ9tJVa4yn8eOfPi/bdJzu7VmH9RxySYlnxImCN/EVXcSRPPPQxtjFxza/+aXM3dvRtsGfBuxfBB9YLsKR9RP6sqLG1hB+oXkjxfDDhNLdF3uMsDNy4GGJGFHewgATWnF5xXWDugOq9asb";
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;

        vuforia = ClassFactory.getInstance().createVuforia(parameters);
    }

    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
            "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);

        tfodParameters.minimumConfidence = 0.3;
    }

    public void tensorDrive(double speed,
                          double time, double goalAngle) {

        if (opModeIsActive()) {

            zeus.motorRight.setPower(speed);
            zeus.motorLeft.setPower(speed);

            zeus.motorRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            zeus.motorLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

            runtime.reset();
            while (opModeIsActive() && runtime.seconds() < time) {

                List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                if (updatedRecognitions != null) {
                    if(updatedRecognitions.size() != 0) {
                        for (Recognition recognition : updatedRecognitions) {
                            if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                                angle = recognition.estimateAngleToObject(AngleUnit.DEGREES);
                            }
                        }

                        double error = goalAngle - angle;
                        integralError += error;
                        double deltaError = error - lastError;

                        double Pterm = kP * error;
                        double Iterm = kI * integralError;
                        double Dterm = kD * deltaError;

                        double correction = Pterm + Iterm + Dterm;
                        correction = Math.min(0.2, Math.max(-0.2, correction));

                        zeus.motorRight.setPower(speed + correction);
                        zeus.motorLeft.setPower(speed - correction);

                        lastError = error;

                        telemetry.addData("Angle ", angle);
                        telemetry.update();
                    } else {
                        zeus.motorRight.setPower(Math.abs(speed));
                        zeus.motorLeft.setPower(Math.abs(speed));
                    }
                }
            }
            zeus.motorRight.setPower(0);
            zeus.motorLeft.setPower(0);
        }
    }
}
