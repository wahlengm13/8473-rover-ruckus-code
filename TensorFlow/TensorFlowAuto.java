package org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.TensorFlow;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus;

import java.security.acl.AclNotFoundException;
import java.util.List;

import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.DRIVE_SPEED;
import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.SLOW_DRIVE_SPEED;
import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.kD;
import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.kI;
import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.kP;

@TeleOp(name = "Tensor Flow", group = "Test")
@Disabled
public class TensorFlowAuto extends LinearOpMode {

    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";

    private HardwareZeus zeus = new HardwareZeus();
    private ElapsedTime runtime = new ElapsedTime();

    private VuforiaLocalizer vuforia;
    private TFObjectDetector tfod;

    int goldMineralX = -1;
    double angle = 20.0;

    private double lastError, integralError;
    private double goalAngle = 0.0;

    @Override
    public void runOpMode() {

        initVuforia();

        zeus.init(hardwareMap);

        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        }

        telemetry.addData("Ready", "Press > to start");
        telemetry.update();

        zeus.motorRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        zeus.motorLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        waitForStart();

        if (opModeIsActive()) {
            if (tfod != null) {
                tfod.activate();
            }

            while (opModeIsActive()) {
                if (tfod != null) {
                    List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                    if (updatedRecognitions != null) {
                      telemetry.addData("# Object Detected", updatedRecognitions.size());
                      if (updatedRecognitions.size() != 0) {
                          for (Recognition recognition : updatedRecognitions) {
                              if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                                  goldMineralX = (int) recognition.getLeft();
                                  angle = recognition.estimateAngleToObject(AngleUnit.DEGREES);
                              } else if (recognition.getLabel().equals(LABEL_SILVER_MINERAL)) {
                                  telemetry.addData("Silver", "Ball");
                              }
                          }
                          boolean angleTester = Math.min(0.5, Math.max(-0.5, angle)) == angle;
                          if (angle == 20.0) {
                              telemetry.addData("CAN NOT SEE", "THINE CUBE");
                          }
                              if (angle > 0.5) {
                                  telemetry.addData("Turn Left", angle);
                                  zeus.motorRight.setPower(-0.2);
                                  zeus.motorLeft.setPower(0.2);

                              } else if (angle < -0.5) {
                                  telemetry.addData("Turn Right", angle);
                                  zeus.motorRight.setPower(0.2);
                                  zeus.motorLeft.setPower(-0.2);
                              } else if (angleTester){
                              zeus.motorRight.setPower(DRIVE_SPEED);
                              zeus.motorLeft.setPower(DRIVE_SPEED);
                              runtime.reset();
                              while (runtime.seconds() < 8.0) {
                                  telemetry.addData("We Driving", "Skrt Skrt");
                                  telemetry.update();
                              }
                              zeus.motorRight.setPower(0.0);
                              zeus.motorLeft.setPower(0.0);
                          }
                      }
                      telemetry.update();
                    }
                }
            }
        }

        if (tfod != null) {
            tfod.shutdown();
        }
    }

    private void initVuforia() {
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = "AWydOn3/////AAAAGWB2YP4r2ERKmLdFMt7DzdUYnt2f97VKdK1fMvb8c5p8iGeDLgwB9dic+osr9GAHQK3K4uJV/8yxon7KXrJNbgzKN82yuHucjwS7gmWkItkoSB+nTn/66dfKF6OyRhh7vBtZqg70Tpv3Pq75kIeij++F34cQNAA3fWEzIoPnuQkew/QP1NNjyZtnIY4lYZFEHgljmtmIP7qwM5vw5pIQRriTaDAfwWPJ9tJVa4yn8eOfPi/bdJzu7VmH9RxySYlnxImCN/EVXcSRPPPQxtjFxza/+aXM3dvRtsGfBuxfBB9YLsKR9RP6sqLG1hB+oXkjxfDDhNLdF3uMsDNy4GGJGFHewgATWnF5xXWDugOq9asb";

        vuforia = ClassFactory.getInstance().createVuforia(parameters);

    }

    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
            "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }

    private void getAngle(List<Recognition> test) {
        for (Recognition recognition : test) {
            if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                goldMineralX = (int) recognition.getLeft();
                angle = recognition.estimateAngleToObject(AngleUnit.DEGREES);
            }
        }
    }
}
