package org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus;

import android.support.annotation.NonNull;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.CLAW_CLOSED;
import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.CLAW_OPEN;
import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.DRIVE_SPEED;
import static org.firstinspires.ftc.teamcode.TeamCodeRoverRuckus.HardwareZeus.FAST_DRIVE_SPEED;

@TeleOp(name = "Driving", group = "Match")
//@Disabled
public class RoverRuckusTeleOp extends OpMode {

    private HardwareZeus zeus = new HardwareZeus();
    private Wait wait = new Wait();

    private int currentPosition = 1;
    private int currentControl = 0;
    private int currentSpeed = 1;
    private DriverControls control;
    private DrivingSpeeds speed;

    private static DriverControls[] controls = new DriverControls[] {DriverControls.NORMAL, DriverControls.INVERTED};
    private static DrivingSpeeds[] speeds = new DrivingSpeeds[] {DrivingSpeeds.DRIVE_FAST, DrivingSpeeds.DRIVE_SLOW};

    @Override
    public void init() {
        zeus.init(hardwareMap);

        zeus.motorRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        zeus.motorLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        control = controls[currentControl];
        speed = speeds[currentSpeed];

        telemetry.addData("你好!", "Press > to start");
        telemetry.update();

    }

    @Override
    public void start() {
        zeus.runtime.reset();
    }

    @Override
    public void loop() {

        //Lift
        float lift = -gamepad2.left_stick_y;
        lift = Range.clip(-lift, -1, 1);
        if (gamepad2.y) {
            zeus.motorLeft.setPower(1.0);
        } else {
            zeus.motorLift.setPower(Range.clip(lift, -.6, .6));
        }

        //Slider
        if (gamepad2.a) {
            zeus.motorSlider.setPower(0.5);
        } else if (gamepad2.b) {
            zeus.motorSlider.setPower(-0.5);
        } else {
            zeus.motorSlider.setPower(0.0);
        }

        //Latch
        if (gamepad2.x) {
            zeus.motorLatch.setPower(0.5);
        } else {
            zeus.motorLatch.setPower(0.0);
        }

        //Intake
        if (gamepad2.left_trigger > 0.0 && gamepad2.right_trigger == 0.0) {
            zeus.motorIntake.setPower(-1.0);
        } else if (gamepad2.right_trigger > 0.0 && gamepad2.left_trigger == 0.0) {
            zeus.motorIntake.setPower(1.0);
        } else {
            zeus.motorIntake.setPower(0.0);
        }

        //Driving
        double CURRENT_SPEED = speed == DrivingSpeeds.DRIVE_FAST ? FAST_DRIVE_SPEED : DRIVE_SPEED;

        float right = -gamepad1.right_stick_y;
        float left = -gamepad1.left_stick_y;

        right = Range.clip(-right, -1, 1);
        left = Range.clip(-left, -1, 1);

        if (gamepad1.right_bumper) {
            speedCycle();
        }

        if (gamepad1.left_bumper) {
            controlCycle();
        }

        switch (control) {
            case NORMAL:
                zeus.motorRight.setPower(Range.clip(right, -CURRENT_SPEED, CURRENT_SPEED));
                zeus.motorLeft.setPower(Range.clip(left, -CURRENT_SPEED, CURRENT_SPEED));
                break;
            case INVERTED:
                zeus.motorRight.setPower(Range.clip(-left, -CURRENT_SPEED, CURRENT_SPEED));
                zeus.motorLeft.setPower(Range.clip(-right, -CURRENT_SPEED, CURRENT_SPEED));
                break;
            default:
                break;
        }
    }

    //Driving
    private void controlCycle() {
        switch (currentControl) {
            case 0:
                currentControl ++;
                control = controls[currentControl];
                break;
            case 1:
                currentControl --;
                control = controls[currentControl];
                break;
            default:
                break;
        }
        wait.waitMilliseconds(250);
    }
    private void speedCycle() {
        switch (currentSpeed) {
            case 0:
                currentSpeed ++;
                speed = speeds[currentSpeed];
                break;
            case 1:
                currentSpeed --;
                speed = speeds[currentSpeed];
                break;
            default:
                break;
        }
        wait.waitMilliseconds(250);
    }

    private enum DriverControls {
        NORMAL, INVERTED
    }

    private enum DrivingSpeeds {
        DRIVE_FAST, DRIVE_SLOW
    }
}
